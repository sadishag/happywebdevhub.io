$(function(){
    "use strict";

    window.bbApp = {
        Views: {},
        Router: {},
        Model: {},
        Collection: {}
    };

    window.bbApp.vent = _.extend({}, Backbone.Events);

    window.bbApp.getImgArray = function(path, n){
        var tempArr = [];
        for( var i = 1; i <= n; i++ ){
            tempArr.push(path + i + '.jpg');
        }
        return tempArr;
    };

    window.bbApp.Model.Project = Backbone.Model.extend({
        default: {
            name: 'Имя проекта',
            content: 'Дополнительная информация о проекте',
            img: 'img link',
            tag: ['Верстка', 'Программирование'],
            imgPak: null
        }
    });

    window.bbApp.Views.Project = Backbone.View.extend({
        tagName: 'li',
        className: 'project-item',
        template: _.template(
            '<div class="img"><img src=<%=img%> alt=""/></div>' +
            '<div class="text">' +
            '<div class="content">' +
            '<div class="title">' +
            '<a href="<%=this.model.url()%>"><%=name%></a>' +
            '<span class="tag">' +
            '<a href="" style="background: #658c10;" class="no-link"><%=tag[0]%></a>' +
            '<% if(tag[1]){%><a href="" class="no-link" style="background: #0544c5;"><%=tag[1]%></a><%}%>' +
            '</span>' +
            '</div>' +
            '<div class="meta"><%=content%></div>' +
            '</div></div>'
        ),
        render: function(){
            this.$el.html(this.template(this.model.toJSON()));
            //this.drag();
            return this;
        }
    });

    window.bbApp.Collection.Project = Backbone.Collection.extend({
        model: window.bbApp.Model.Project,
        url: '#!'
    });

    window.bbApp.Views.ProjectImgPak = Backbone.View.extend({
        className: 'fotorama',
        //el: '.fotorama',
        // template: _.template('<% this.gen() %>'),
        gen: function(){
            var imgArr = this.model.get('imgPak');
            var str = '';

            for(var i = 0; i < imgArr.length; i++){
                str += '<img alt="" src="' + imgArr[i] + '"/>';
            }

            return str;
        },
        render: function(){
            var gen = this.gen();
            this.$el.html(_.template(gen));
            return this;
        }
    });

    window.bbApp.Views.ProjectPak = Backbone.View.extend({
        tagName: 'ul',
        className: "work",
        initialize: function(){
            window.bbApp.vent.on('goPage', this.show, this);
        },
        show: function(query){

            //console.log(query);
            //if(query >= 0 && query <= 5)
            $(this.$el.fadeOut());
            //$($('.fotorama').append)
            //window.sds = this;

            var fotorama = new window.bbApp.Views.ProjectImgPak({
                model: this.collection.at(query)
            });

            fotorama.render();
            $('#paste').append(fotorama.$el);
            //$(fotorama.$el.fotorama());
            $(function () {
                $('.fotorama').fotorama();
            });
            //window.a = fotorama;
            //console.log(fotorama);
        },
        render: function(){
            this.$el.append("<div class='title-head'>Работы</div>");
            this.collection.each(this.addOne, this);
            //window.tt = this.collection.at(2);
            return this;
        },
        addOne: function(task, index){
            var project = new window.bbApp.Views.Project({
                model: task
            });
            //task.id = 'project_' + index;
            task.id = '' + index;
            //console.log(task.url());
            //window.task = task;
            //window.project = project;

            project.render();

            if (task.has('left')){
                project.$el.find('img').css({
                    left: task.get('left')
                });
            }

            this.$el.append(project.el);
        }
    });

    window.bbApp.Router.ProjectRouter = Backbone.Router.extend({
        routes:{
            '': 'startRender',
            '!/': 'refresh',
            '!/:query': 'datFunctionForProject'
        },
        domDance: function(){
            $('.work').fadeIn();
            $('.return').hide();
            this.fotoramaRemove();   
        },
        fotoramaRemove: function(){
            $('.fotorama').remove();
            $('.fotorama--hidden').remove();
            $('style','#paste').remove(); 
        },
        startRender: function(){
            console.log(new Date());
            this.domDance();
        },
        refresh: function(){
            //console.log('[goto]\t\t\t\t/');
            this.domDance();
        },
        datFunctionForProject: function(query){
            console.log('goto: ' + query);
            this.fotoramaRemove();
            window.bbApp.vent.trigger('goPage', query);
            $('.return').show();
            //$('.fotorama').fotorama();
        }
    });

    var projectPak = new window.bbApp.Collection.Project([
        {
            name: 'UNLOCKPRO',
            content: 'UNLOCKPRO — профессиональная разблокировка iPhone',
            img: './image/pro.png',
            tag: ['Адаптивная верстка'],
            left: 11,
            imgPak: window.bbApp.getImgArray('./image/project/unlockpro/', 4)

        },
        {
            name: 'Бетар компани',
            content: 'Магазин высококачественных медицинских изделий.',
            img: './image/betar.png',
            tag: ['Верстка'],
            imgPak: window.bbApp.getImgArray('./image/project/betar/', 18)
        },
        {
            name: 'Поток Силы',
            content: 'Школы авторского Таро «Поток Силы».',
            img: './image/potok.png',
            tag: ['Верстка', 'Программирование'],
            left: 34,
            imgPak: window.bbApp.getImgArray('./image/project/potok/', 9)
        },
        {
            name: 'Elitrem',
            content: 'Ремонт бытовой техники.',
            img: './image/elitrem.png',
            tag: ['Верска', 'Программирование'],
            imgPak: window.bbApp.getImgArray('./image/project/elitrem/', 5)
        },
        {
            name: 'Engels.bz',
            content: 'Информационный портал города Энгельс.',
            img: './image/engels.png',
            tag: ['Верстка', 'Программирование'],
            left: 14,
            imgPak: window.bbApp.getImgArray('./image/project/engels/', 4)
        },
        {
            name: 'MilitaryModel',
            content: 'Интернет магазин маштабных моделей.',
            img: './image/mm.png',
            tag: ['Верстка','Программирование'],
            imgPak: window.bbApp.getImgArray('./image/project/mm/', 3)
        }
    ]);
    var projects = new window.bbApp.Views.ProjectPak({
        collection: projectPak
    });

    $('#paste').append(projects.render().$el);
    new window.bbApp.Router.ProjectRouter();
    Backbone.history.start();
});

