module.exports = function(grunt){
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		jshint: {
          options: {
            curly: true,
            eqeqeq: true,
            immed: true,
            latedef: true,
            newcap: true,
            noarg: true,
            sub: true,
            undef: true,
            eqnull: true,
            browser: true,
            globals: {
              jQuery: true,
              $: true,
              console: true,
              Backbone: true,
              _: true
            }
          },
          'jshint': {
            src: [ 'src/js/main.js' ]
          }
        },
        browserDependencies: {
        	options:{},
        	jquery:{
        		dir: "src/js/lib/jquery",
        		files:[
        			{
        				"jquery-1.10.2.js": "http://code.jquery.com/jquery-1.10.2.min.js"
        			}
        		]
        	},
        	underscore:{
        		dir: "src/js/lib/underscore",
        		files:[
        			{
        				"underscore-1.5.2.js": 'http://underscorejs.org/underscore-min.js'
        			}
        		]
        	},
			backbone: {
		        dir: "src/js/lib/backbone",
		        files: [
					{
						"backbone-1.1.0.js": "http://backbonejs.org/backbone-min.js"
					}
		        ]
	        },
	        fotorama:{
	        	dir: "src/js/lib/fotorama",
	        	files: [
	        		{
	        			"fotorama-4.4.8.js": "http://fotorama.s3.amazonaws.com/4.4.8/fotorama.js"
	        		}
	        	]
	        }
	    },
		concat: {
			main: {
				src: ['src/js/lib/jquery/*.js', 'src/js/lib/underscore/*.js', 'src/js/lib/backbone/*.js', 'src/js/lib/fotorama/*.js', 'src/js/main.js'],
				dest: 'build/script.js'
			}
		},

		uglify:{
			options:{
				stripBanners: true, 
				banner: '/* Author: sporke, <%= grunt.template.today("yyyy-mm-dd") %>, <%= pkg.name %> - v<%= pkg.version %>*/\n'
			},
			main: {
				files:{
					'build/script.min.js':'<%= concat.main.dest %>'
				}
			}
		},
		watch:{},
		cssmin: { 
            with_banner: {
                options: {
                    banner: '/* My minified CSS */' 
                },
 
                files: {
                    'build/style.min.css' : 'src/css/noclear.css'
                }
            }
        },

	});

	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	grunt.registerTask('default', ['jshint','browserDependencies','concat', 'uglify', 'cssmin']);
};